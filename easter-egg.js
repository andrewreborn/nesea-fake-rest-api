const message = `
    Ciao ragazzi (:\r\n
    Molti di voi staranno pensando che è davvero una giornataccia questa, eh?\r\n
    Niente di più sbagliato. Concentratevi al massimo e fate del vostro meglio:\r\n
    credo nelle capacità di ognuno di voi. Ricordate tutte le cose sulle quali\r\n
    ci siamo soffermati ad apportare correzioni e per cui vi ho rotto le scatole.\r\n
    \r\n
    SIATE POSITIVI, ANDRÀ UNA BOMBA.\r\n
    \r\n
    Andrew <3
`;

console.log("\x1b[32m", message);