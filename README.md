# Come preparare l'API

- Clonate questo repository in una cartella **diversa** da quella del vostro progetto Angular: ricordate che sono due cose ben distinte, questo è un backend ed il vostro è il frontend!
- Posizionatevi da terminale all'interno della cartella dell'API
- Installate le dipendenze con il comando **npm install**


# Come usare l'API

Fate partire l'applicazione attraverso il comando **npm run start** e lasciate la scheda del terminale aperta: il processo dovrà rimanere attivo affinchè possa rispondere alle richieste che farete! - Quando l'API parte, in console c'è un messaggio per voi (: - Ricordate che il server gira all'indirizzo **http://localhost:3000** e potete verificarne il funzionamento accedendo a questo indirizzo dal vostro browser.

## Consultare l'API

Ci sono due diversi endpoint da utilizzare per l'esercitazione:
- categories - eroga un elenco di **categorie**
- products - restituisce un elenco di **prodotti**

Per recuperare i dati dovrete interrogare gli endpoint agli indirizzi:
- http://localhost:3000/categories
- http://localhost:3000/products


## Filtrare le informazioni

Potete eseguire richieste a ciascun endpoint aggiungendo dei filtri. Poniamo il caso che vogliate recuperare i prodotti appartenenti ad una sola categoria...
- Il campo che contiene l'id della categoria all'interno di ciascun prodotto è nomenclato "category_id"
- Utilizzo le query string sull'endpoint di mio interesse mettendo il simbolo **?** al termine dell'indirizzo, aggiungendo poi il campo con il valore da associare

Se volessi recuperare tutti i prodotti presenti all'interno della categoria "Musica" che ha id 1, dovrò filtrare i prodotti per il campo category_id con valore 1 in questo modo:
http://localhost:3000/products?category_id=1
